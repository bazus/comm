<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/{name}', 'ComsController@category');			//view category

Route::post('/category/{categ}/{id}', 'ComsController@cont_categ');	// live loading script for category

Route::get('/', 'ComsController@index');					//index page

Route::post('/cont/{id}', 'ComsController@cont');			// live loading script for main



Route::get('/post/{id}', 'ComsController@post');	
Route::post('/create', 'ComsController@create');			//new comment				

