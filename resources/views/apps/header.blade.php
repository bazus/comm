
<header>
  
  <div class="body-container head">
    <a href="/" class="logo">
      <img class="big_logo lazy" title="Сайт отзывов" src="img/logo.png" alt="logo">
      <img class="small_logo lazy" title="Сайт отзывов" src="img/small_logo.svg" alt="logo">
    </a>
    <b>Первый независимый сайт отзывов Украины</b>
    <div class="controlls">
      <i class="far fa-search"></i>
      <i class="far fa-align-justify" data-toggle="dropdown">

         <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="#">Action</a>
  <a class="dropdown-item" href="#">Another action</a>
  <a class="dropdown-item" href="#">Something else here</a>
  <div class="dropdown-divider"></div>
  <a class="dropdown-item" href="#">Separated link</a>
        </div>
      </i>
      <button class="addComm"><i class="fal fa-pencil"></i>Написать отзыв</button>
    </div>
  </div>
</header>

 
<div class="modal-add">
  <div class="modal-helper"></div>
  <div class="modal-window">
    <i id="modal-close" class="far fa-times"></i>
    <div class="comment-header"><h2>Новый отзыв</h2></div>
    <div class="comment-body">
      <div class="star"><i id="1" class="fas fa-star str"></i>
      <i id="2" class="fas fa-star str"></i>      
      <i id="3" class="fas fa-star str"></i>
      <i id="4" class="fas fa-star str"></i>
      <i id="5" class="fas fa-star str"></i>
      </div>

      <form id ="contact_form" class="contact_form" role="form" method="post" action="create">
             {!! csrf_field() !!}

              <input id="mark" type="hidden" name="mark" max="5">
              <div class="form-group form-about">
                <i class="fal fa-info-circle"></i>
                <input id="title" type="text" class="form-control" placeholder="О чём ваш отзыв?" name='title'>
              </div>
              <div class="form-group form-about">
            <i class="fab fa-typo3"></i>
            <select id="category" data-placeholder="tessss" class="js-example-basic-single" name="category">
              <option value="0">Выберите категорию</option>
              @foreach($categories as $category)
              <option value="{{$category->id}}">{{$category->rus_name}}</option>
              @endforeach
              
            </select>
          </div>
              <div class="form-group form-about">
                <i class="fal fa-comments"></i>
                <input id="text-title" type="text" class="form-control" placeholder="Заголовок отзыва" name="text-title">
              </div>

            

            <div class="form-group form-about">
              <i class="far fa-comment"></i>
              <textarea type="text" class="form-control" id="text" placeholder="Отзыв" name="text"></textarea>
            </div>
            <div class="form-group form-about">

          <div class="custom-control custom-checkbox">
            <input name="checkbox" type="checkbox" class="custom-control-input comm-cheked" id="customCheck1">
            <label class="custom-control-label" for="customCheck1">Указать плюсы и минусы</label>
        </div>
            </div>
            <div class="form-group form-about">
              <i class="far fa-smile"></i>
              <textarea id="positive" name="positive" class="positive" type="text" placeholder="Положительные"></textarea>
              
            </div>
            <div class="form-group form-about">
              <i class="far fa-frown"></i>
              <textarea id="negative" name="negative" class="negative" type="text" placeholder="Отрицательные"></textarea>
            </div>
            
            <div class="form-group form-about">
                <i class="far fa-user-circle"></i>
                <input id="author" type="text" class="form-control" id="text-title" placeholder="Автор" name="author">
            </div>
          <div style="display: none" class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Ошибка!  </strong>Заполнены не все поля!
          </div>
          <div style="display: none" class="alert alert-success" role="alert">
         <strong>Успешно!  </strong>Отзыв отправлен!
        </div>
            <a id="send_message"  class="btn btn-primary">Отправить</a>

        </form>




    </div>

  </div>
</div>
