<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>

    <meta charset="utf-8">
<meta name="description" content="Otzyv.ua - независимый сайт отзывов">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{setting('site.title')}}</title>

   


    <link rel="shortcut icon" href="img/fav.ico">
    <link rel="stylesheet" href="libs/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="libs/webfonts/all.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="libs/slider/slide.css">
  <link rel="stylesheet" href="libs/select/css/select.css" rel="stylesheet" />

  <script src="libs/iquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
                


  <script type="text/javascript" src="libs/slider/slide.js"></script>
  <script type="text/javascript" src="libs/massonry.js"></script> 
 

  </head>
  <body>

@include('apps.header')







<div class="body-container galery">
  <div class="main-carousel">
    @foreach($categories as $category)
    <div class="carousel-cell"><a href="{{$category->name}}"><label class="item-scroll">{{$category->rus_name}}     </label></a></div>
    @endforeach
  </div>
  <div class="pd15">
    <div class="info">
      <i class="fal fa-comments"></i>
      <div class="info-text">
    <b>Сайт отзывов</b> создан с целью повышения эффективности и качества предоставляемых услуг и товаров. Каждый день наш сайт посещают сотни тысяч пользователей, которые формируют определенное мнение о той или иной услуге или товаре. Каждый день мы стараемся сделать сервис лучше. Благодарим за сотрудничество!</div>
    </div>
  </div>
    <div id="all_box">


      @foreach($posts as $post)
       <div id="{{$post->id}}" class="item_box">

              <div class="item">
                <div class="post-head">
                  <div class="post-img">
                    <a href="post/{{$post->id}}">
                      <div class="item-img">
                        <img class="lazy" width="100" data-src="{{$post->img}}" alt="img">
                      </div>
                    </a>
                  </div>
    
                  <div class="post-title">
                    <h2>{{$post->theme}}</h2>
                    <div class="title-description">
                      {{$categories[$post->category -1]->rus_name}}
                    </div>
                    <div class="marc">
                        @for($i = 0; $i < 5; $i++)
                        
                          @if($i < $post->mark)
                          <i class="fas fa-star"></i>
                        @else
                          <i class="fal fa-star"></i>
                        @endif
                          
                      @endfor
                    </div>
                    <div class="coments">
                      81 отзыв
                    </div>
                    <div class="views">
                      1 338 просмотров
                    </div>
                  </div>
                </div>
                <div class="post-body">
                  <a href="post/{{$post->id}}">
                    <strong>{{$post->title}}</strong><br>
                    <span>{{$post->body}}
    
                    </span>
    
                  </a>
    
                </div>
                <div class="post-footer writer">
                  <b>{{$post->author}}</b><br>
                  <small>{{$post->created_at}}</small>
                </div>
    
              </div>
            </div>

      @endforeach
      
     
    </div>
</div>

<div class="load">
   <div class="page-loader-circle"></div>
</div>
<div class="top-pusher">
  <i class="fas fa-arrow-square-up"></i>
</div>







<script>
  var refresh_st = "category/{{$posts[0]->category}}";
  function categ(i){
    switch(i){
      <?php
        foreach ($categories as $categ) {
          echo "case ".$categ->id.":return '".$categ->rus_name."';break;
          ";
        }
      ?>
      default: return " ";
    }

  }
</script>


@include('apps.footer')



  </body>
</html>
