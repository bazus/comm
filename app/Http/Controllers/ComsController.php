<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComsController extends Controller
{
    
    public function index(){
        $categories =\App\Category::get(); 
    	$posts = \App\Com::orderBy('id','DESC')->take(40)->get();
    	return view('index', compact('posts', 'categories'));
    }
    public function cont($id){
    	$number = count(\App\Com::all()) - $id + 1;
        $posts = DB::select("SELECT * FROM `coms` where  id < ".$id." order by id desc limit 40");
    	return $posts;
    }
     public function cont_categ($categ, $id){
        $posts = DB::select("SELECT * FROM `coms` where category = ".$categ." and  id < ".$id." order by id desc limit 40" );

        return $posts;
    }

    public function create(Request $req){

    		if($req->file('image') == NULL){
    			$img = "user/default.jpg";
    		} else {
            	$img = "storage/".$req ->file('image')->store('uploads', 'public');
            }


              $fill = \App\Com::insert(array(
                'category' => $req->input('category'),
                'mark' => $req->input('mark'),
                'img' => $img,
                'theme' => $req->input('title'),
                'title' => $req->input('text-title'),
                'body' => $req->input('text'), 
                'positive' => $req->input('positive'),
                'negative' => $req->input('negative'),
                'author' => $req->input('author')
            ));
              return 1;
        
    }

    public function category($name){
         $id = DB::select('select id from category where name = "'.$name.'"');
         $posts = DB::select('select * from coms where category = "'.$id[0]->id.'" order by id DESC limit 40');
         $categories =\App\Category::get(); 
        return view('category', compact('posts', 'categories'));
    }
    public function post ($id){
        $categories =\App\Category::get(); 
        $post = DB::select('select * from coms where id = '.$id.'');
        return view('post', compact('post', 'categories'));
    }
    public function img(Request $req){
        $path = $req ->file('image')->store('uploads', 'public');
        $text = $req->input('text');
        return view('img', compact('path', 'text'));
    }


}
